<?php get_header(); ?>

<div class="main-container">
    <div class="main wrapper clearfix">


        <?php if(have_posts()): ?>

            <?php while(have_posts())
            {
                the_post();

                the_title('<h1>', '</h1>');

                the_content('<p>', '</p>');

            } ?>

        <?php else: ?>

            Er is geen inhoud gevonden.

        <?php endif; ?>


            <!-- hier komt sidebar staat al in sidebar.php-->
            <?php get_sidebar(); ?>

    </div> <!-- #main -->
</div> <!-- #main-container -->

<?php get_footer(); ?>
