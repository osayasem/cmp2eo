<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'cmp2eo');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'bitnami');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'u|LKoLDdI30FztasVkR[en~<PhS>^55#r!sJ+}L%PjzDfSN(RAE;1Il^hMz52o7g');
define('SECURE_AUTH_KEY',  'l1Cjd1p3@|AF)DOVFxcn()hz ^BI<7P|QL~xEiJH,lM2*Xq.B>D)!]H1_6s_23*r');
define('LOGGED_IN_KEY',    '[F!S?2,NTX]<!kF$BSr$4TI=9c$j%U>^/l G^0Ps))a%wVillO>(&fm#RM0Rf*#{');
define('NONCE_KEY',        'bun<,aPXH5fm|R?bFg1=ii,c;[mjHV?{7P+G1-5%Pcv-:8Pr)2tC:2)%{h^s|XG<');
define('AUTH_SALT',        'hzBh7#d6f,.o|_6Puu{ ux{d0{K5~=_6SuV{?P >o<&1L+An7#6Ux(P9/0<;.jc&');
define('SECURE_AUTH_SALT', ';I%wl-xmU34[&.3b:WtMXV778V*1FKIYgXmhN h8D27/R+szS}1W-i8z oC%]kix');
define('LOGGED_IN_SALT',   '@-+g{iQr4bVdcUREJI63AM,L<<MgF~u8-/jg(=5([%>w5z/?||Hw`51w01ff 41L');
define('NONCE_SALT',       '~5!.roglqik9HRM%d5`<iVfJYq3 H/7h(=<60MHimC_l^gpNl^64&Jb_Er:F6ml:');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
