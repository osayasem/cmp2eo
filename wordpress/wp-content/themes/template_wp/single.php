<?php if(have_posts()): ?>

    <?php while(have_posts())
    {
        the_post();

        the_title('<h1>', '</h1>');

        the_content('<p>', '</p>');
        the_author();
        the_date();

    } ?>

<?php else: ?>

    Er is geen inhoud gevonden.

<?php endif; ?>

comments_template();
